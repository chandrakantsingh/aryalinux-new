#!/bin/bash

set -e
set +h

. /sources/build-properties
. /sources/build-functions

NAME=068-systemd

touch /sources/build-log
if ! grep "$NAME" /sources/build-log; then

cd /sources

TARBALL=systemd-246.tar.gz
DIRECTORY=$(tar tf $TARBALL | cut -d/ -f1 | uniq)

tar xf $TARBALL
cd $DIRECTORY


ln -sf /bin/true /usr/bin/xsltproc
tar -xf ../systemd-man-pages-246.tar.xz
sed '177,$ d' -i src/resolve/meson.build
sed -i 's/GROUP="render", //' rules.d/50-udev-default.rules.in
mkdir -p build
cd       build

LANG=$LOCALE                    \
meson --prefix=/usr                 \
      --sysconfdir=/etc             \
      --localstatedir=/var          \
      -Dblkid=true                  \
      -Dbuildtype=release           \
      -Ddefault-dnssec=no           \
      -Dfirstboot=false             \
      -Dinstall-tests=false         \
      -Dkmod-path=/bin/kmod         \
      -Dldconfig=false              \
      -Dmount-path=/bin/mount       \
      -Drootprefix=                 \
      -Drootlibdir=/lib             \
      -Dsplit-usr=true              \
      -Dsulogin-path=/sbin/sulogin  \
      -Dsysusers=false              \
      -Dumount-path=/bin/umount     \
      -Db_lto=false                 \
      -Drpmmacrosdir=no             \
      -Dhomed=false                 \
      -Duserdb=false                \
      -Dman=true                    \
      -Ddocdir=/usr/share/doc/systemd-246 \
      ..
LANG=$LOCALE ninja
LANG=$LOCALE ninja install
rm -f /usr/bin/xsltproc
systemd-machine-id-setup
systemctl preset-all
systemctl disable systemd-time-wait-sync.service
rm -f /usr/lib/sysctl.d/50-pid-max.conf

fi

cleanup $DIRECTORY
log $NAME
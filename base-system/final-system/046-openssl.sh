#!/bin/bash

set -e
set +h

. /sources/build-properties
. /sources/build-functions

NAME=046-openssl

touch /sources/build-log
if ! grep "$NAME" /sources/build-log; then

cd /sources

TARBALL=openssl-1.1.1h.tar.gz
DIRECTORY=$(tar tf $TARBALL | cut -d/ -f1 | uniq)

tar xf $TARBALL
cd $DIRECTORY


ln -svf /tools/bin/env /usr/bin/
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic
make
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install
mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1h
cp -vfr doc/* /usr/share/doc/openssl-1.1.1h

fi

cleanup $DIRECTORY
log $NAME
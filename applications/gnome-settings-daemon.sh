#!/bin/bash

set -e
set +h

. /etc/alps/alps.conf
. /var/lib/alps/functions
. /etc/alps/directories.conf

#REQ:colord
#REQ:fontconfig
#REQ:gcr
#REQ:geoclue2
#REQ:geocode-glib
#REQ:gnome-desktop
#REQ:lcms2
#REQ:libcanberra
#REQ:libgweather
#REQ:libnotify
#REQ:librsvg
#REQ:libwacom
#REQ:pulseaudio
#REQ:elogind
#REQ:upower
#REQ:xorg-wacom-driver
#REQ:alsa
#REQ:cups
#REQ:networkmanager
#REQ:nss
#REQ:wayland
#REQ:blocaled


cd $SOURCE_DIR

wget -nc http://ftp.gnome.org/pub/gnome/sources/gnome-settings-daemon/3.38/gnome-settings-daemon-3.38.1.tar.xz
wget -nc ftp://ftp.acc.umu.se/pub/gnome/sources/gnome-settings-daemon/3.38/gnome-settings-daemon-3.38.1.tar.xz


NAME=gnome-settings-daemon
VERSION=3.38.1
URL=http://ftp.gnome.org/pub/gnome/sources/gnome-settings-daemon/3.38/gnome-settings-daemon-3.38.1.tar.xz

if [ ! -z $URL ]
then

TARBALL=$(echo $URL | rev | cut -d/ -f1 | rev)
if [ -z $(echo $TARBALL | grep ".zip$") ]; then
	DIRECTORY=$(tar tf $TARBALL | cut -d/ -f1 | uniq | grep -v "^\.$")
	sudo rm -rf $DIRECTORY
	tar --no-overwrite-dir -xf $TARBALL
else
	DIRECTORY=$(unzip_dirname $TARBALL $NAME)
	unzip_file $TARBALL $NAME
fi

cd $DIRECTORY
fi

echo $USER > /tmp/currentuser


sed -e 's/libsystemd/libelogind/' \
    -i plugins/power/test.py
sed -i 's/(backlight->logind_proxy)/(0)/' \
    -i plugins/power/gsd-backlight.c
mkdir build &&
cd    build &&

meson --prefix=/usr -Dsystemd=false .. &&
ninja
sudo rm -rf /tmp/rootscript.sh
cat > /tmp/rootscript.sh <<"ENDOFROOTSCRIPT"
ninja install
ENDOFROOTSCRIPT

chmod a+x /tmp/rootscript.sh
sudo /tmp/rootscript.sh
sudo rm -rf /tmp/rootscript.sh



if [ ! -z $URL ]; then cd $SOURCE_DIR && cleanup "$NAME" "$DIRECTORY"; fi

register_installed "$NAME" "$VERSION" "$INSTALLED_LIST"

